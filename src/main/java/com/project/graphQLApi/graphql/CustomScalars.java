package com.project.graphQLApi.graphql;

import graphql.language.StringValue;
import graphql.schema.*;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.DateTimeException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.function.Function;

public class CustomScalars {
    public static final GraphQLScalarType LOCAL_DATE_TIME = GraphQLScalarType.newScalar().name("LocalDateTime")
            .description("Scale for DateTime").coercing(new Coercing<LocalDateTime, String>() {
        @Override
        public String serialize(Object dataFetcherResult) throws CoercingSerializeException {
            LocalDateTime localDateTime;
            if(dataFetcherResult instanceof  LocalDateTime){
                localDateTime = (LocalDateTime) dataFetcherResult;
            }else if(dataFetcherResult instanceof String){
                    localDateTime = this.parseLocalDataTime(dataFetcherResult.toString(), CoercingParseLiteralException::new);
            }else{
                throw  new CoercingSerializeException("Canoot convert to LocatDateTime");
            }

            try {
                return DateTimeFormatter.ISO_LOCAL_DATE_TIME.format(localDateTime);
            }catch (DateTimeException e){
                throw new CoercingSerializeException("Invalid Date");
            }

        }

        @Override
        public LocalDateTime parseValue(Object input) throws CoercingParseValueException {
            LocalDateTime localDateTime;
            if(input instanceof  LocalDateTime){
                localDateTime = (LocalDateTime) input;
            }else if(input instanceof  String){
                localDateTime =  this.parseLocalDataTime(input.toString(), CoercingParseLiteralException::new);
            }else{
                throw new CoercingParseValueException("Cannot convert to LocalDateTime");
            }

            return localDateTime;
        }

        @Override
        public LocalDateTime parseLiteral(Object input) throws CoercingParseLiteralException {
            if(!(input instanceof StringValue)){
                throw new CoercingParseLiteralException("Cnnot conver to LocalDateTime");
            }

            return this.parseLocalDataTime(((StringValue) input).toString(), CoercingParseLiteralException::new);

        }

        private LocalDateTime parseLocalDataTime(String s, Function<String, RuntimeException> exceptionMaker){
            try{
                return LocalDateTime.parse(s);
            }catch (DateTimeParseException e){
                throw exceptionMaker.apply("Ivalid value");
            }
        }
    }).build();
}
