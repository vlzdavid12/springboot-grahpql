package com.project.graphQLApi.graphql;

import graphql.GraphQL;
import graphql.schema.GraphQLSchema;
import graphql.schema.idl.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.IOException;

@Component
public class GraphQlProvider {

    @Autowired
    private GraphQlDataFetchers graphQlDataFetchers;

    @Value("classpath:schema.graphqls")
    private Resource resource;

    private GraphQL graphQL;

    @PostConstruct
    public void init() throws IOException {
        GraphQLSchema  graphQLSchema =  this.buildSchema();
        this.graphQL = GraphQL.newGraphQL(graphQLSchema).build();
    }

    private GraphQLSchema buildSchema() throws IOException {
        final TypeDefinitionRegistry typeRegister =  new SchemaParser().parse(resource.getInputStream());
        SchemaGenerator schemaGenerator = new SchemaGenerator();
        return schemaGenerator.makeExecutableSchema(typeRegister, this.buildWriting());
    }

    private RuntimeWiring buildWriting(){
        return RuntimeWiring.newRuntimeWiring()
                .scalar(CustomScalars.LOCAL_DATE_TIME)
                .type(TypeRuntimeWiring.newTypeWiring("Query").dataFetcher("findAll",
                        this.graphQlDataFetchers.getFindAllDataFetcher()))
                .type(TypeRuntimeWiring.newTypeWiring("User").dataFetcher("post",
                        this.graphQlDataFetchers.getPostsByUserFetcher()))
                .type(TypeRuntimeWiring.newTypeWiring("Mutation").dataFetcher("addUser",
                        this.graphQlDataFetchers.getAddUserFetcher()))
                .type(TypeRuntimeWiring.newTypeWiring("Mutation").dataFetcher("updateUser",
                        this.graphQlDataFetchers.getUpdateUserFetcher()))
                .type(TypeRuntimeWiring.newTypeWiring("Mutation").dataFetcher("deleteUser",
                        this.graphQlDataFetchers.getDeleteUserFetcher()))
                .type(TypeRuntimeWiring.newTypeWiring("Mutation").dataFetcher("addPost",
                        this.graphQlDataFetchers.getAddPostFetcher()))
                .build();
    }

    @Bean
    public GraphQL graphQL(){
        return  this.graphQL;
    }


}
