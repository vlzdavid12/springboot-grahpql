package com.project.graphQLApi.graphql;
import com.project.graphQLApi.services.PostService;
import org.springframework.beans.factory.annotation.Autowired;

import com.project.graphQLApi.objects.inputs.PostInput;
import com.project.graphQLApi.objects.inputs.UserInput;

import com.project.graphQLApi.objects.types.Post;
import com.project.graphQLApi.objects.types.User;


import com.project.graphQLApi.services.UserService;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Component;

import graphql.schema.DataFetcher;

import java.util.List;

@Component
public class GraphQlDataFetchers {

    @Autowired
    private UserService userService;

    @Autowired
    private PostService postService;


    public DataFetcher<List<User>> getFindAllDataFetcher() {
        return dataFetchingEnvironment -> this.userService.findAll();
    }

    public DataFetcher<User> getAddUserFetcher() {
        return dataFetchingEnvironment -> {
            final UserInput userInput = new ObjectMapper()
                    .convertValue(dataFetchingEnvironment.getArgument("userInput"), UserInput.class);
            return this.userService.save(userInput);
        };
    }

    public DataFetcher<User> getUpdateUserFetcher() {
        return dataFetchingEnvironment -> {
            UserInput userInput = new ObjectMapper().convertValue(dataFetchingEnvironment.getArgument("userInput"), UserInput.class);
            Integer userId = dataFetchingEnvironment.getArgument("userId");

            return this.userService.update(userId, userInput);
        };
    }

    public DataFetcher<User> getDeleteUserFetcher() {
        return dataFetchingEnvironment -> {
            Integer userId = dataFetchingEnvironment.getArgument("userId");
            return this.userService.delete(userId);
        };
    }

    public DataFetcher<Post> getAddPostFetcher() {
        return dataFetchingEnvironment -> {
            final PostInput postInput = new ObjectMapper()
                    .convertValue(dataFetchingEnvironment.getArgument("postInput"), PostInput.class);
            return this.postService.save(postInput);
        };
    }

    public DataFetcher<List<Post>> getPostsByUserFetcher(){
        return dataFetchingEnvironment -> {
            final User user = dataFetchingEnvironment.getSource();
            return this.postService.getPostByUser(user.getId());
        };
    }
}
