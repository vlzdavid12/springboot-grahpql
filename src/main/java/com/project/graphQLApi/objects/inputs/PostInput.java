package com.project.graphQLApi.objects.inputs;

public class PostInput {
    private String post;
    private Integer userId;

    public String getPost() {
        return post;
    }

    public void setPost(String post) {
        this.post = post;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }
}
