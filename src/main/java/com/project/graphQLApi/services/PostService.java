package com.project.graphQLApi.services;

import com.project.graphQLApi.objects.inputs.PostInput;
import com.project.graphQLApi.objects.types.Post;

import java.util.List;

public interface PostService {
    List<Post> getPostByUser(Integer userId);

    Post save(PostInput postInput);
}
