package com.project.graphQLApi.services.Impl;

import org.springframework.beans.factory.annotation.Autowired;
import com.project.graphQLApi.entitys.UserEntity;
import com.project.graphQLApi.mappers.UserMapper;

import com.project.graphQLApi.objects.types.User;
import com.project.graphQLApi.reposiroty.UserRepository;
import com.project.graphQLApi.services.UserService;

import com.project.graphQLApi.objects.inputs.UserInput;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;


@Service
public class UserDBServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;
    @Override
    public List<User> findAll() {
           return userRepository.findAll().stream().map(UserMapper::entityToUser).collect(Collectors.toList());
    }

    @Override
    public User save(UserInput userInput) {
        return UserMapper.entityToUser(this.userRepository.save(UserMapper.inputToEntity(userInput)));
    }

    @Override
    public User update(Integer id, UserInput userInput) {
        UserEntity userEntity = UserMapper.inputToEntity(userInput);
        userEntity.setId(id);
        return UserMapper.entityToUser(this.userRepository.save(userEntity));
    }

    @Override
    public User delete(Integer id) {
        User user =  userRepository.findById(id).map(UserMapper:: entityToUser).orElse(null);
        this.userRepository.deleteById(id);
        return user;
    }
}
