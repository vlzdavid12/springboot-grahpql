package com.project.graphQLApi.services.Impl;

import com.project.graphQLApi.mappers.PostMapper;
import com.project.graphQLApi.objects.inputs.PostInput;
import com.project.graphQLApi.objects.types.Post;
import com.project.graphQLApi.reposiroty.PostRepository;
import com.project.graphQLApi.services.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class PostDBServiceImpl implements PostService {
    @Autowired
    private PostRepository postRepository;

    @Override
    public List<Post> getPostByUser(Integer userId) {
        return this.postRepository.findByUserId(userId).stream().map(PostMapper::entityToPost).collect(Collectors.toList());
    }

    @Override
    public Post save(PostInput postInput) {
        return PostMapper.entityToPost(postRepository.save(PostMapper.inputToPost(postInput)));
    }
}
