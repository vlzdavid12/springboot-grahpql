package com.project.graphQLApi.services;

import com.project.graphQLApi.objects.inputs.UserInput;
import com.project.graphQLApi.objects.types.User;
import org.springframework.stereotype.Service;

import java.util.List;

public interface UserService {

   List<User> findAll();

   User save(UserInput userInput);

   User update(Integer id, UserInput userInput);

   User delete(Integer id);

}
