package com.project.graphQLApi.reposiroty;

import com.project.graphQLApi.entitys.PostEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PostRepository extends  JpaRepository<PostEntity, Integer> {
    List<PostEntity> findByUserId(Integer userId);
}
