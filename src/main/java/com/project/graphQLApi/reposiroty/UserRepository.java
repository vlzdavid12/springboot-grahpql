package com.project.graphQLApi.reposiroty;

import com.project.graphQLApi.entitys.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<UserEntity, Integer> {
}
