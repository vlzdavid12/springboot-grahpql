package com.project.graphQLApi.mappers;

import com.project.graphQLApi.entitys.UserEntity;
import com.project.graphQLApi.objects.inputs.UserInput;
import com.project.graphQLApi.objects.types.User;

public class UserMapper {
    public static UserEntity userToEntity(User user){
        UserEntity entity =  new UserEntity();
        entity.setId(user.getId());
        entity.setName(user.getName());
        entity.setUsername(user.getUsername());
        entity.setEmail(user.getEmail());
        return entity;
    }

    public static UserEntity inputToEntity(UserInput userInput){
        final UserEntity entity =  new UserEntity();
        entity.setName(userInput.getName());
        entity.setUsername(userInput.getUsername());
        entity.setEmail(userInput.getEmail());
        return entity;
    }

    public static User entityToUser(UserEntity entity){
        final User user =  new User();
        user.setId(entity.getId());
        user.setName(entity.getName());
        user.setUsername(entity.getUsername());
        user.setEmail(entity.getEmail());
        return user;
    }


}
