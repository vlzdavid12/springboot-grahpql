package com.project.graphQLApi.mappers;

import com.project.graphQLApi.entitys.PostEntity;
import com.project.graphQLApi.objects.inputs.PostInput;
import com.project.graphQLApi.objects.types.Post;

public class PostMapper {

    public static Post entityToPost(PostEntity entity){
        final Post post =  new Post();
        post.setId(entity.getId());
        post.setPost(entity.getPost());
        post.setPublishDate(entity.getPublishDate());
        return post;
    }

    public static PostEntity inputToPost(PostInput postInput){
        final PostEntity post =  new PostEntity();
        post.setUserId(postInput.getUserId());
        post.setPost(postInput.getPost());
        return post;
    }
}
